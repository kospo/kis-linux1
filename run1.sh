#!/bin/bash

if [ "$#" -ne "3" ]; then
        echo "usage: $0 <backuped dir> <comma-separated extensions> <target archive name>"
        exit 1
fi

backup_dir=$(realpath $1)
extensions=$2
arch_path=$(realpath $3)

arch_file="$arch_path.tar"
arch_dir="$arch_path/"

mkdir -p $arch_dir

ext_regex=$(sed -e 's/,/|/g' <<< $extensions)
ext_regex=".*\.($ext_regex)"

find "$backup_dir" \
        -regextype posix-extended \
        -regex "$ext_regex" \
        -exec cp --parents '{}' "$arch_dir" \; > /dev/null 2>&1 && \
tar -cf "$arch_file" \
        -C "$(dirname $arch_dir)" \
        "$(basename $arch_dir)" > /dev/null 2>&1 && \
openssl enc -e \
        -aes-256-cbc \
        -pbkdf2 \
        -in "$arch_file" \
        -out "$arch_file.sec" && \
rm -rf "$arch_dir" "$arch_file"

echo "done"